package com.example.demo;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.get;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

class TestGetPlayer {

	@BeforeEach
	void setUp() throws Exception {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
	}

	@Test
	void test() {
		given().log().all()
	      .when().get("http://localhost:8080/getPlayer/102").then().statusCode(200).assertThat()
	      .body("id", equalTo(103));
		
		/*given().pathParam("id", 101)
	      .when().get("/getPlayer/{id}")
	      .then().statusCode(200).assertThat().body("id", equalTo(101));*/
		
		/*Response response = given().
		contentType("application/json").
		accept("\\*\\/*").
		body("").
		when().
		get("/getPlayer/102").
		then().
		statusCode(200).
		contentType("application/json").
		extract().
		response();*/
		
		
		
		//assertTrue("eDigi".equals(response.jsonPath().getString("name")));
		
		
	}

}
