package com.floorball.parent.exceptions;

public class PlayerNotFoundException extends RuntimeException{
	public PlayerNotFoundException(String errorMessage) {
		super(errorMessage);
	}
}
