package com.floorball.parent.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.floorball.parent.entity.Player;

public interface PlayerRepository extends JpaRepository<Player, Integer>{
	@Query("SELECT u.name FROM Player u")
	ArrayList<String> getPlayersNames();
}
