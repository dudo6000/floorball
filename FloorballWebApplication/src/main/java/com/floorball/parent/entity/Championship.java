package com.floorball.parent.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
public class Championship {

	@Id
	@GeneratedValue
	private int id;
	
	private String name;
	
	@OneToMany(mappedBy = "championship", cascade = CascadeType.ALL)
	private Set<Team> teams;

	public Championship() {
		super();
	}

	public Championship(int id, String name, Set<Team> teams) {
		super();
		this.id = id;
		this.name = name;
		this.teams = teams;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Team> getTeams() {
		return teams;
	}

	public void setTeams(Set<Team> teams) {
		this.teams = teams;
	}
	
    @Override
    public String toString() {
        String result = String.format(
                "Team[id=%d, name='%s']%n",
                id, name);
        if (teams != null) {
            for(Team team : teams) {
                result += String.format(
                        "Team[id=%d, name='%s']%n",
                        team.getId(), team.getName());
            }
        }

        return result;
    }
}
