package com.floorball.parent.entity;

import com.floorball.parent.services.CalculatorService;

public class Calculator {
	CalculatorService calculatorService;

	public Calculator(CalculatorService calculatorService) {
		this.calculatorService = calculatorService;
	}
	
	public int perform(int i, int j) {
		return calculatorService.add(i,j)*2;
	}
}
