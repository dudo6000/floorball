package com.floorball.parent.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.hibernate.dialect.MySQLStorageEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.floorball.parent.entity.Player;
import com.floorball.parent.interfaces.InterfaceForDefaultMethod;
import com.floorball.parent.repository.PlayerRepository;

@RestController
public class DummyController {
	
	@GetMapping("/myDefaultInterfaceTester/{list}")
	public ArrayList<Integer> myDefaultInterfaceTester(@PathVariable ArrayList<Integer> list) {
		InterfaceForDefaultMethod obj = new InterfaceForDefaultMethod() {
			
		};
		return (ArrayList<Integer>) list.stream().map(obj::myDeafaultMethod).collect(Collectors.toList());
	}
	
	
	@GetMapping("/myFunctionalInterfaceTester/{list}")
	public ArrayList<Integer> myFunctionalInterfaceTester(@PathVariable ArrayList<Integer> list) {
				//egy saját kis inetrface ide jönne elméletileg a sort
		return null;
	}
	
	@GetMapping("/sortStringByLength/{list}")
	public ArrayList<String> sortEndpoint(@PathVariable ArrayList<String> list) {
		Comparator<String> compByLength = (aString, bString) -> aString.length() - bString.length();
		return (ArrayList<String>) list.stream().sorted(compByLength).collect(Collectors.toList());
	}
	
	
	@GetMapping("/myStaticInterfaceTester/{list}")
	public ArrayList<Integer> myStaticInterfaceTester(@PathVariable ArrayList<Integer> list) {
				
		return (ArrayList<Integer>) list.stream().map(InterfaceForDefaultMethod::myStaticMethod).collect(Collectors.toList());
		//return list.stream().map(InterfaceForDefaultMethod::myStaticMethod).collect(Collectors.toList(ArrayList<Integer>::new));
	}
	
	
}

