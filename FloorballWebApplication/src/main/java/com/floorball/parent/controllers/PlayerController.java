package com.floorball.parent.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.floorball.parent.entity.Player;
import com.floorball.parent.exceptions.PlayerNotFoundException;
import com.floorball.parent.interfaces.InterfaceForDefaultMethod;
import com.floorball.parent.repository.PlayerRepository;

@RestController
public class PlayerController {
	
	@Autowired
	private PlayerRepository playerRepository;
	
	@PostMapping("/addPlayer")
	public Player addPlayer(@RequestBody Player player) {
		playerRepository.save(player);
		return player;
	}
	
	@GetMapping("/getPlayer/{id}")
	public Optional<Player> getPlayer(@PathVariable Integer id) {
		//System.out.println("*********** : " + playerRepository.findById(id));
		Optional<Player> c = playerRepository.findById(id);
		
		
		System.out.println("*********** : " + c);
		if(c.get().getId()==999) {
			throw new PlayerNotFoundException("Player not found");
		}
		
		return c;
		//return playerRepository.findById(id);
		
	}
	
	@DeleteMapping("/deletePlayer/{id}")
	public String deletePlayer(@PathVariable Integer id) {
		
		Player p = playerRepository.getOne(id);
		playerRepository.delete(p);
		
		return "deleted";
	}
	
	@GetMapping("/getPlayers")
	public List<Player> getPlayers() {
		
		return playerRepository.findAll();
	}

	@GetMapping("/getPlayersNames")
	public ArrayList<String> getPlayersNames() {
		
		return playerRepository.getPlayersNames();
	}
	
	@GetMapping("/getPlayerWithLowestID")
	public Optional<Player> getPlayerWithLowestID() {
		ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();
		
		Optional<Player> result = players.stream()
		.min(Comparator
		.comparing(Player::getId));
		
		return result;
	}
	
	@GetMapping("/getPlayerWithBiggestID")
	public Optional<Player> getPlayerWithBiggestID() {
		ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();
		
		Optional<Player> result = players.stream()
		.max(Comparator
		.comparing(Player::getId));
		
		return result;
	}
	
	
	@GetMapping("/findPlayerByName/{name}")
	public Optional<Player> findPlayerByName(@PathVariable String name) {
		ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();
		
		Optional<Player> result = players.stream()
		.filter(player -> name.equals(player.getName())).findAny();
				
		return result;
	}
	
	@GetMapping("/ownClassSort")
	public ArrayList<Player> ownClassSort() {
		
		ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();
		return (ArrayList<Player>) players.stream().sorted(InterfaceForDefaultMethod::compareByNameThenNumber).collect(Collectors.toList());
	}
}

