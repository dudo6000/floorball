package com.floorball.parent.interfaces;

import com.floorball.parent.entity.Player;

public interface InterfaceForDefaultMethod {
	default int myDeafaultMethod(int i) {
		System.out.println("myFunctionalInterface default method: " + i + " -> " + 2*i);
		return 2*i;
	}
	
	static int myStaticMethod(Integer i) {
		System.out.println("myFunctionalInterface: " + i + " -> " + 4*i);
		return i*4;
	}
	
	public static int compareByNameThenNumber(Player lhs, Player rhs) {
	    if (lhs.getName().equals(rhs.getName())) {
	        return lhs.getNumber() - rhs.getNumber();
	    } else {
	        return lhs.getName().compareTo(rhs.getName());
	    }
	}
}
                     