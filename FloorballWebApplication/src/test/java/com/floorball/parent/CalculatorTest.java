package com.floorball.parent;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.floorball.parent.entity.Calculator;
import com.floorball.parent.services.CalculatorService;

public class CalculatorTest {

	Calculator calculator = null;
	
	
	/*
	 * első megoldás amikor egy saját "mock" készítünk
	 * CalculatorService calculatorService = new CalculatorService() {
		
		@Override
		public int add(int i, int j) {
			// TODO Auto-generated method stub
			return 0;
		}
	};*/
	
	CalculatorService calculatorService = mock(CalculatorService.class);
	// itt lehet még külömböző rule okat is definiálni

	@Before
	public void setUp() {
		when(calculatorService.add(2, 3)).thenReturn(5);
		calculator = new Calculator(calculatorService);
		
	}
			
			
	@Test
	public void test() {
		assertEquals("Calculator test failed!", 10,  calculator.perform(2, 3));
		verify(calculatorService).add(2, 3);
	}

}
