package com.floorball.parent;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.floorball.parent.controllers.DummyController;

public class DefaultAndStaticInterfaceTest {
	
	DummyController dummyController = null;
	
	@Before
	public void setUp() {
		dummyController = new DummyController();
	}

	@Test
	public void defaultMethodTest() {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		
		ArrayList<Integer> resultList = dummyController.myDefaultInterfaceTester(list);
		System.out.println("original list:");
		System.out.println(list);
		
		System.out.println("after calling list:");
		System.out.println(resultList);
		
		
		
		ArrayList<Integer> result = new ArrayList<>();
		result.add(2);
		result.add(4);
		result.add(6);
		
		System.out.println("acceptence list:");
		System.out.println(result);
		
		assertEquals("List are different!", result, resultList);
	}

}
