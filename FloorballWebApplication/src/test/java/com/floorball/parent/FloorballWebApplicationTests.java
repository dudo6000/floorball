package com.floorball.parent;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

//az anystring importját ez az import oldotta meg, statikusan kell beimprotálni
import static org.mockito.Mockito.*;

import com.floorball.parent.controllers.PlayerController;
import com.floorball.parent.entity.Player;
import com.floorball.parent.exceptions.PlayerNotFoundException;
import com.floorball.parent.repository.PlayerRepository;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class FloorballWebApplicationTests {

	@Mock // ez service amit mockolni szeretnénk
	PlayerRepository playerRepository = mock(PlayerRepository.class);
	
	@InjectMocks // így rakjuk az autowired helyére
	PlayerController playerController = new PlayerController();
	
	@Before
	public void setUp() {
		//ha ez nem futna le akkor az autowired service nem működne
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
		Player result = new Player(101, "cDagi", 23);
		Player mock = new Player(101, "cDagi", 23);
		when(playerRepository.findById( anyInt() )).thenReturn(Optional.of(mock));
		
		Optional<Player> c = playerController.getPlayer(101);
		
		int number = c.get().getNumber();
		
		assertEquals("PlayerController test failed!", 23, number);
	}
	
	@Test(expected = PlayerNotFoundException.class)
	public void exceptionTest() {
		when(playerRepository.findById( 999 )).thenReturn(Optional.of(new Player(999, "Bad", 99)));
		
		Optional<Player> c = playerController.getPlayer(999);
		
		/*
		 * junit 5
		 * assertThrows(PlayerNotFoundException.class,
		 * 
		 * () -> {
		 * 		playerController.getPlayer(999);
		 * }
		 * 
		 * );
		 */
	}

}
